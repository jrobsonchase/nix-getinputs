let
  inherit (builtins) isNull isBool isString isFloat isFunction isInt isList
    isPath isAttrs foldl' attrValues map getContext filter attrNames all any elem
    getAttr mapAttrs hasAttr listToAttrs;

  isDerivation = attrs: isAttrs attrs && attrs ? type && attrs.type == "derivation";

  # any/all inversions
  # [(v -> bool)] -> v -> bool
  anyf = fs: value: any (f: f value) fs;
  allf = fs: value: all (f: f value) fs;

  # inverse of listToAttrs
  attrsToList = attrs: map (k: { name = k; value = getAttr k attrs; }) (attrNames attrs);

  # Return a set containing only the attrs matching pred.
  filterAttrs = pred: attrs: listToAttrs (filter (a: pred a.name a.value) (attrsToList attrs));

  # Turn a list of lists into a single list
  flattenList = list: foldl' (acc: l: acc ++ l) [ ] list;

  # Define a thing with contexts as one of:
  #   * An actual input (path or derivation)
  #   * A string with context
  hasContext = anyf [ (allf [ isString builtins.hasContext ]) isPath isDerivation ];

  # Pull the context out of a string and convert derivations/paths to their
  # equivalent context for simplicity.
  resolveContext = value:
    if isString value then getContext value
    else if isDerivation value then getContext "${value}"
    else if isPath value then listToAttrs [{ name = builtins.toString value; value = { path = true; }; }]
    else value;

  # Recrusively find the contexts contained in `value`.
  # Returns a list contexts.
  getDirectInputs =
    let
      getListDirectInputs = list: flattenList (map getDirectInputs list);
      getAttrsDirectInputs = attrs: getListDirectInputs (attrValues attrs) ++ getListDirectInputs (attrNames attrs);
    in
    value:
    if isDerivation value then [ value ]
    else if isList value then getListDirectInputs value
    else if isAttrs value then getAttrsDirectInputs value
    else [ ];

  # Recrusively find the contexts contained in `value`.
  # Returns a list contexts.
  getContexts =
    let
      getListContexts = list: flattenList (map getContexts list);
      getAttrsContexts = attrs: getListContexts (attrValues attrs) ++ getListContexts (attrNames attrs);
    in
    value:
    if hasContext value then [ (resolveContext value) ]
    else if isList value then getListContexts value
    else if isAttrs value then getAttrsContexts value
    else [ ];

  # Predicates for the type of a context item.
  isPathContext = a: a.value ? path && a.value.path;
  isDrvContext = a: a.value ? outputs;

  # Get an attr if it exists, or return a default value
  getAttrOr = attr: set: default:
    if ! hasAttr attr set then default
    else getAttr attr set;

  # Merge a context into the input accumulator. inputSrcs is a list of paths
  # and inputDrvs is a set where the names are drv path strings and values are
  # lists of output names.
  # Deduplicates any inputs already seen.
  mergeContext = { inputSrcs ? [ ], inputDrvs ? { } }: ctx:
    let
      ctxPairs = attrsToList ctx;

      # Paths are easy. Convert to paths and filter out the already extant ones.
      pathContexts = map (i: /. + i.name) (filter isPathContext ctxPairs);
      newPaths = filter (p: ! elem p inputSrcs) pathContexts;

      # Drvs are a bit trickier since the output is a set. Do the union early
      # so that the set merge is simple.
      drvContexts = filter isDrvContext ctxPairs;
      unionDrvContexts = map
        ({ name, value }:
          let existing = getAttrOr name inputDrvs [ ]; in
          {
            inherit name;
            value = existing ++ (filter (o: ! elem o existing) value.outputs);
          })
        drvContexts;
      drvs = listToAttrs unionDrvContexts;
    in
    {
      inputSrcs = inputSrcs ++ newPaths;
      inputDrvs = inputDrvs // drvs;
    };

  drvsFromOutputs = name: outputs:
    let
      path = /. + name;
      drv = import path;
    in
    map (out: getAttr out drv) outputs;

  getInputs = value: assert isDerivation value;
    let
      contexts = getContexts value.drvAttrs;
      merged = foldl' mergeContext { } contexts;
      inputDrvs = flattenList (attrValues (mapAttrs drvsFromOutputs merged.inputDrvs));
    in
    merged // {
      inherit inputDrvs;
    };
in
{
  inherit getInputs getDirectInputs;
}
