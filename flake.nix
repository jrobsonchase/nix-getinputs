{
  description = "Pure nix utility function to get all inputs for a derivation.";

  outputs = { self }: {
    lib = import ./getinputs.nix;
  };
}
